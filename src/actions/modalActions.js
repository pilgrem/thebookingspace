import { TYPE } from './types';

/**
 * Action to open modal.
 * @returns void
 */
export const openModal = () => dispatch => {
    dispatch({ type: TYPE.MODAL_OPEN_STATUS, payload: true })
}

/**
 * Action to close modal.
 * @returns void
 */
export const closeModal = () => dispatch => {
    dispatch({ type: TYPE.MODAL_OPEN_STATUS, payload: false });
}

/**
 * Set's type of Modal form to be used in switch statement elsewhere.
 * @param {string} type 
 * @returns void
 */
export const setModalType = type => dispatch => {
    dispatch({ type: TYPE.MODAL_TYPE, payload: type });
}
