export const TYPE = {
    FETCH_BOOKINGS: 'fetch_bookings',
    NEW_BOOKING: 'new_booking',
    SELECTED_BOOKING_DATA: 'selected_booking_data',
    UPDATE_BOOKING: 'update_booking',
    UPDATE_DISPLAYED_BOOKINGS: 'update_displayed_bookings',
    DELETE_BOOKING: 'delete_booking',
    MODAL_OPEN_STATUS: 'modal_open_status',
    MODAL_TYPE: 'modal_type',
    EMAIL_SEND: 'email_send',
    GATEWAY_SET_ACCESS: 'gateway_set_access',
    GATEWAY_GET_ACCESS: 'gateway_get_access',
    GATEWAY_PAGE_LOADED: 'gateway_page_loaded',
}
