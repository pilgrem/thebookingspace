import { TYPE } from './types';
import { collection, getDocs, addDoc, query, where } from 'firebase/firestore';
import {db} from '../firebase-config';
import { randomString, getCookieValue } from '../common/helper';


async function saveToken(token, date) {
    const tokenRef = collection(db, "tokens");
    const data = {
        date,
        token,
    }
    await addDoc(tokenRef, data)
        .catch(err => console.error(err));
}

/**
 * Action to get Gateway access.
 * @returns void
 */
export const getAccess = () => async (dispatch) => {

    const tokenRef = collection(db, "tokens");
    let cookieToken = getCookieValue('accessToken');
    cookieToken = typeof cookieToken !== 'undefined' ? cookieToken : false;

    if (cookieToken) {
        const tokenQuery = query(tokenRef, where('token', '==', cookieToken));

        await getDocs(tokenQuery).then((response) => {
            
            if (response.docs.length) {
                response.docs.map((doc) => {                
                    const { token } = doc.data();
                    const response = cookieToken === token;
                    dispatch({ type: TYPE.GATEWAY_GET_ACCESS, payload: response });
                    return true;
                });
            } else {
                dispatch({ type: TYPE.GATEWAY_GET_ACCESS, payload: false })
            }
            
        })
        .catch(err => {
            console.error(err);
            dispatch({ type: TYPE.GATEWAY_GET_ACCESS, payload: false });
        });
    } else {
        dispatch({ type: TYPE.GATEWAY_GET_ACCESS, payload: false });
    }
}

/**
 * Set Gateway Access.
 * @returns void
 */
export const setAccess = userCode => async (dispatch) => {
    // Set Access Token Cookie
    const gatewayRef = collection(db, "gateway");
    await getDocs(gatewayRef)
        .then( (response) => {
            let correctCode = false;
            response.docs.map((doc) => {
                const code = doc.data().code;
                correctCode = code === userCode;
                return correctCode;
            })
            
            if (correctCode) {
                const expiration = new Date(new Date().getTime() + 30 * 1000 );
                const token = randomString();
                document.cookie = `accessToken=${token};expires:${expiration}`;
              
                // Save token to database to check for later.
                saveToken(token, expiration.toString());
            } 
            dispatch({ type: TYPE.GATEWAY_SET_ACCESS, payload: response })           
        })
        .catch(err => {
            console.error(err);
            dispatch({ type: TYPE.GATEWAY_SET_ACCESS, payload: false })
        });    
}

/**
 * Page is loaded?
 * @returns void
 */
 export const pageLoaded = () => dispatch => {
    dispatch({ type: TYPE.GATEWAY_PAGE_LOADED, payload: true })
}
