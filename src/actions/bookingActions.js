import { TYPE } from './types';
import { addDoc, collection, getDocs, updateDoc, doc, deleteDoc } from 'firebase/firestore';
import {db} from '../firebase-config';

/**
 * Fetch all Booking data from Firebase.
 * @returns void
 */
export const fetchBookings = () => async (dispatch) => {
    const bookingsCollectionRef = collection(db, "bookings");
    await getDocs(bookingsCollectionRef).then((response) => {
        let bookings = [];

        response.docs.map((doc) => {
            const bookingsData = doc.data();
            bookings = [...bookings, {
                id: doc.id,
                title: bookingsData.title,
                start: new Date(bookingsData.start),
                end: new Date(bookingsData.end),
                attendees: JSON.parse(bookingsData.attendees),                
            }];

            return bookings;
        });

        dispatch({ type: TYPE.FETCH_BOOKINGS, payload: bookings });
    })
    .catch(err => console.error(err));
}
    
/**
 * Creates a new bookingItem entry in Firebase.
 * @param {object} bookingItem 
 * @returns void
 */
export const createBooking = bookingItem => async (dispatch) => {
    const bookingsCollectionRef = collection(db, "bookings");
    const data =  {
        title: bookingItem.title,
        start: bookingItem.start.toString(),
        end: bookingItem.end.toString(),
        attendees: JSON.stringify(bookingItem.attendees),
    }
    await addDoc(bookingsCollectionRef, data).then( () => {
        dispatch({ type: TYPE.NEW_BOOKING, payload: bookingItem })
    })
    .catch(err => console.error(err));
}

/**
 * Updates a bookingItem based on specific id.
 * @param {object} bookingItem 
 * @returns void
 */
export const updateBooking = bookingItem => async (dispatch) => {
    const bookingDoc = doc(db, "bookings", bookingItem.id);
    const newFields =  {
        title: bookingItem.title,
        start: bookingItem.start.toString(),
        end: bookingItem.end.toString(),
        attendees: JSON.stringify(bookingItem.attendees),
    }
    await updateDoc(bookingDoc, newFields).then( () => {
        dispatch({ type: TYPE.UPDATE_BOOKING, payload: true })
    })
    .catch(err => {
        console.error(err);
        dispatch({ type: TYPE.UPDATE_BOOKING, payload: false })
    });
}

/**
 * Deletes a booking item via id.
 * @param {object} bookingItem 
 * @returns 
 */
export const deleteBooking = bookingItem => async (dispatch) => {
    const bookingDoc = doc(db, "bookings", bookingItem.id);
    await deleteDoc(bookingDoc).then( () => {

        // Send success flag.
        dispatch({ type: TYPE.DELETE_BOOKING, payload: true });        
    })
    .catch(err => {
        console.error(err);
        dispatch({ type: TYPE.DELETE_BOOKING, payload: false});
    });
}

/**
 * Used to update state of current booking items.
 */
export const updateDisplayedBookings = (bookingItems, updateData, remove = false) => dispatch => {

    try {
        const id = updateData.id;
        if (!id) throw new Error('Update Booking ID not found, Calendar items will not be updated.');


        if (!remove) {
            // Update booking item in current state list of booking items based on id.
            const index = bookingItems.findIndex((obj => obj.id === id));        
            bookingItems[index] = updateData;
        } else {
            // Remove booking item from current state list of booking items.
            const newBookingItems = bookingItems.filter(item => {
                return item.id !== id;
            });         
            bookingItems = newBookingItems;
        }
        
        dispatch({ type: TYPE.UPDATE_DISPLAYED_BOOKINGS, payload: bookingItems });
    } catch (error) {
        console.error(error);
    }
}

export const selectedBookingData = bookingData => dispatch => {
    dispatch({ type: TYPE.SELECTED_BOOKING_DATA, payload: bookingData });
}