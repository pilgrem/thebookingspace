import { TYPE } from './types';
import Email from '../common/email';

/**
 * Action to send email.
 * @returns void
 */
 export const sendEmail = state => dispatch => {

    try {
        if (!Object.entries(state).length) throw new Error('State not found for sending emails.');

        const attendeeList = state.attendees;
        if (attendeeList.length) {
            attendeeList.forEach(attendee => {
                
                const start = new Date(state.start).toLocaleString();
                const end = new Date(state.end).toLocaleString();

                if (attendee.email.length) {
                    const email = new Email();
                        email.attendees = state.attendees;            
                        email.to = attendee.email;
                        email.name = attendee.name;
                        email.title = state.title;
                        email.startTime = start;
                        email.endTime = end;
                        email.sendMail();
                }            
            }); 
        }      

        dispatch({ type: TYPE.EMAIL_SEND, payload: true })
    } catch(error) {
        console.error(error);
        dispatch({ type: TYPE.EMAIL_SEND, payload: false })
    }        
}