import { TYPE } from '../actions/types';

const initialState = {
    status: false
};

const emailReducer = (state = initialState, action) => {
    switch (action.type) {
        case TYPE.EMAIL_SEND:
            return {
                ...state,
                status: action.payload
            }
        default:
            return state;
    }
}

export default emailReducer;