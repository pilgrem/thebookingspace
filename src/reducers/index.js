import { combineReducers } from "redux";
import bookingReducer from "./bookingReducer";
import modalReducer from "./modalReducer";
import emailReducer from "./emailReducer";
import gatewayReducer from './gatewayReducer';

export default combineReducers({
    bookings: bookingReducer,
    modal: modalReducer,
    email: emailReducer,
    gateway: gatewayReducer,
});