import { TYPE } from '../actions/types';

const initialState = {
    bookingUpdated: null,
    bookingDeleted: null,
    bookingItems: [],
    bookingItem: {
        id: null,
        title: '',
        start: new Date(),
        end: new Date(),
        attendees: [],
    },    
    selectedBookingData: {
        id: null,
        title: '',
        start: new Date(),
        end: new Date(),
        attendees: [],
    }
};

const bookingReducer = (state = initialState, action) => {
    switch (action.type) {
        case TYPE.FETCH_BOOKINGS:
            return {
                ...state,
                bookingItems: action.payload
            }
        case TYPE.NEW_BOOKING:
            return {
                ...state,
                bookingItems: [...state.bookingItems, action.payload]
            }
        case TYPE.DELETE_BOOKING:
            return {
                ...state,
                bookingDeleted: action.payload
            }
        case TYPE.UPDATE_BOOKING:
            return {
                ...state,
                bookingUpdated: action.payload
            }
        case TYPE.UPDATE_DISPLAYED_BOOKINGS:
            return {
                ...state,
                bookingItems: action.payload
            }
        case TYPE.SELECTED_BOOKING_DATA:
            return {
                ...state,
                selectedBookingData: action.payload
            }
        default:
            return state;
    }
}

export default bookingReducer;