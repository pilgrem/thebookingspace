import { TYPE } from '../actions/types';

const initialState = {
    modalOpen: false,
    modalType: '',
};

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case TYPE.MODAL_OPEN_STATUS:
            return {
                ...state,
                modalOpen: action.payload
            }
        case TYPE.MODAL_TYPE:
            return {
                ...state,
                modalType: action.payload
            }
        default:
            return state;
    }
}

export default modalReducer;