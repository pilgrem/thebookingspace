import { TYPE } from '../actions/types';

const initialState = {
    access: null,
    pageLoaded: null,
};

const gatewayReducer = (state = initialState, action) => {
    switch (action.type) {
        case TYPE.GATEWAY_GET_ACCESS:
            return {
                ...state,
                access: action.payload
            }
        case TYPE.GATEWAY_SET_ACCESS:
            return {
                ...state,
                access: action.payload
            }
        case TYPE.GATEWAY_PAGE_LOADED:
            return {
                ...state,
                loaded: action.payload
            }
        default:
            return state;
    }
}

export default gatewayReducer;