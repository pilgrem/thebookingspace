import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import { connect } from 'react-redux';
import { createBooking } from '../actions/bookingActions';
import { closeModal } from '../actions/modalActions';
import { sendEmail } from '../actions/emailActions';
import Attendee from '../common/attendee';
import { setFormMessage } from '../common/helper';
import Validation from '../common/validation';

class BookingForm extends Component {

    initialState = {
        title: '',
        start: '',
        end: '',
        attendees: [],
        sendNotifications: false,
        redirect: false,
    };

    constructor(props) {
        super(props);

        this.state = this.initialState;

        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeDateStart = this.onChangeDateStart.bind(this);
        this.onChangeDateEnd = this.onChangeDateEnd.bind(this);
        this.onAttendeeAdd = this.onAttendeeAdd.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onNotificationCheck = this.onNotificationCheck.bind(this);
        this.handleAttendeeChange = this.handleAttendeeChange.bind(this);
    }
    
    componentDidMount() {
        this.setAttendeeChangeListener();
    }

    setAttendeeChangeListener() {
        const attendees = document.querySelectorAll('.attendee-item');
        attendees.forEach(attendee => {
            attendee.addEventListener("change", this.handleAttendeeChange);
        });
    }

    onAttendeeAdd(e) {
        e.preventDefault();
        const attendee = new Attendee();
        const attendeeList = e.target.previousSibling;
        const attendeeListCount = attendeeList.children.length;
        attendee.parentObject = attendeeList;
        attendee.attendeeCount = attendeeListCount;
        attendee.add();
        this.setAttendeeChangeListener();
    }

    handleAttendeeChange(e) {
        const form = e.target.form;

        // Build attendee object for state.
        const attendees = [];        
        let emptyInputData = 0;
        Object.keys(form.elements).forEach(key => {
            let element = form.elements[key];
            if (element.type === 'fieldset') {
                const fieldSetElements = element.children;
                const inputData = {};
                
                Object.values(fieldSetElements).map((input) => {
                    if (!input.value.length) {
                        emptyInputData++;
                    }
                    inputData[input.dataset.key] = input.value;
                    return inputData;
                });

                if (emptyInputData !== 2) {
                    attendees.push(inputData);    
                }                
            }
            emptyInputData = 0;
        });
       
        this.setState({attendees});
    }

    onChangeTitle(e) {
        this.setState({title: e.target.value});
    }

    onChangeDateStart(start) {
        this.setState({start});
    }

    onChangeDateEnd(end) {
        this.setState({end});
    }

    onNotificationCheck(e) {
        const isChecked = e.target.checked;
        this.setState({sendNotifications: isChecked});
    }

    /**
     * Clears status message and resets state for form.
     * @param {object} form 
     */
    clearForm(form) {
        form.classList.remove('submission-updated');
        form.querySelector('.submission-message').remove();
        this.setState(() => this.initialState);
    }

    async onSubmit(e) {
        e.preventDefault();
        const form = e.target;
        const validationError = new Validation(this.state);
        const errorContainer = form.querySelector('.booking-error-validation');

        if (validationError.length) {
            // Add error messaging.
            if (!errorContainer.classList.contains('error')) {
                errorContainer.classList.add('error');
            }
            errorContainer.innerText = validationError[0];
        } else {
            // Clean up errors.
            if (errorContainer.classList.contains('error')) {
                errorContainer.classList.remove('error');
                errorContainer.innerText = '';
            }

            const redirect = typeof this.props.redirect !== 'undefined' ? this.props.redirect : false;
            await this.props.createBooking(this.state)
                .then(() => {                
                    setFormMessage(form, 'Booking Created!');

                    // Coniditionally Send email to all attendees.
                    if (this.state.sendNotifications) {
                        this.props.sendEmail(this.state);
                        const emailSendStatus = this.props.emailStatus;
                        if (!emailSendStatus) {
                            setFormMessage(form, 'Emails Failed to send. Contact Administrator.');
                        }                 
                    }                
                })
                .catch(err => {
                    setFormMessage(form, 'Your booking was not created due to an error.');
                    console.error(err);                
                })
                .finally(() => {            
                    setTimeout(() => {
                        if (redirect) {                            
                            this.setState({redirect: true});
                        }
                        this.props.closeModal();
                        this.clearForm(form);                                                
                    }, 1350);                    
                });
        }                
    }

    render() {
        return(
            <div className="booking-form">
                <form onSubmit={this.onSubmit}>
                    <h2>Enter booking information: </h2>
                    <p className="booking-error-validation"></p>
                    <div className="form-element">
                        <label htmlFor="booking-title">Title:<sup>*</sup></label>
                        <input
                            id="booking-title"
                            type="text" 
                            placeholder="Enter Booking Title"
                            name="booking-title"
                            value={this.state.title}
                            onChange={this.onChangeTitle}
                        />
                    </div>
                                  
                    <div className="form-element">
                        <label htmlFor="start-date-picker">Start:<sup>*</sup></label>
                        <DatePicker
                            id='start-date-picker'
                            placeholderText="Start Date/Time"
                            showTimeSelect
                            dropdownMode="select"
                            dateFormat="M/d/Y h:mm aa"                
                            selected={this.state.start}
                            onChange={this.onChangeDateStart}
                        />
                    </div>
                    
                    <div className="form-element">
                        <label htmlFor="end-date-picker">End:<sup>*</sup></label>
                        <DatePicker
                            id='end-date-picker'
                            placeholderText="End Date/Time"
                            showTimeSelect
                            dropdownMode="select"
                            dateFormat="M/d/Y h:mm aa"                    
                            selected={this.state.end}
                            onChange={this.onChangeDateEnd}
                        />
                    </div>
                    
                    <div className="booking-attendees">
                        <p>Invite Attendees:</p>
                        <div className="booking-attendee-list">
                            <fieldset name="attendee-group-0">
                                <input className="attendee-item" type="text" name="attendee-name[0]" placeholder="Name" data-key="name"/>
                                <input className="attendee-item" type="email" name="attendee-email[0]" placeholder="Email" data-key="email"/>
                            </fieldset>
                        </div>
                        <button onClick={this.onAttendeeAdd} className="add-attendee">+</button>
                    </div>

                    <div className="form-element">
                        <label htmlFor="attendee-notifications" className="checkbox">
                            <input 
                                type="checkbox"
                                name="attendee-notifications" 
                                id="attendee-notifications"
                                onClick={this.onNotificationCheck}
                            />
                            Send Notifications to Attendees?</label>
                    </div>
                                        
                    <button type="submit" >Add Booking</button>
                </form>
                {
                    this.state.redirect
                    ? <Redirect to="/calendar"/>
                    : null
                }
            </div>            
        )
    }
}

BookingForm.propTypes = {
    createBooking: PropTypes.func,
    closeModal: PropTypes.func,
    redirect: PropTypes.bool,
    sendEmail: PropTypes.func,
}

const mapStateToProps = state => ({
    emailStatus: state.email.status,
});

export default connect( mapStateToProps, { createBooking, closeModal, sendEmail } )(BookingForm);