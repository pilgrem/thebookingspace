import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchBookings, selectedBookingData } from '../actions/bookingActions';
import { openModal, setModalType } from '../actions/modalActions';
import { Calendar, dateFnsLocalizer } from 'react-big-calendar';
import format from 'date-fns/format';
import parse from 'date-fns/parse'
import startOfWeek from 'date-fns/startOfWeek';
import getDay from 'date-fns/getDay';
import CalendarModal from './CalendarModal';
import 'react-big-calendar/lib/css/react-big-calendar.css';

class BookingCalendar extends Component {

    constructor(props) {
        super(props);

        this.onSelectBooking = this.onSelectBooking.bind(this);
        this.handleAddBooking = this.handleAddBooking.bind(this);

        const locales = {
            "en-US": require("date-fns/locale/en-US")
        }

        this.state = {
            localizer: dateFnsLocalizer({
                format,
                parse,
                startOfWeek,
                getDay,
                locales
            }),
        };
    }

    componentDidMount() {
        this.props.fetchBookings();
    }

    onSelectBooking(e) {
        this.props.selectedBookingData(e);
        this.props.setModalType('displayBooking');
        this.props.openModal();
    }

    handleAddBooking() {
        this.props.setModalType('bookingForm');
        this.props.openModal();
    }

    render() {
        
        const { bookingItems } = this.props;

        return(
            <div className="booking-calendar">
                <button className="add-booking" onClick={this.handleAddBooking}>+ Add Booking</button>
                <Calendar
                    localizer={this.state.localizer}
                    events={bookingItems}
                    selectable={true}
                    popup={true}
                    defaultView="month"
                    defaultDate={new Date()}
                    startAccessor="start"
                    endAccessor="end"
                    onSelectEvent={this.onSelectBooking}
                    style={{height: 600, margin: "50px"}}
                />
                <CalendarModal />
             
            </div>
        );
    }
}

BookingCalendar.propTypes = {
    fetchBookings: PropTypes.func.isRequired,
    selectedBookingData: PropTypes.func,
    openModal: PropTypes.func,
    setModalType: PropTypes.func,
    bookingItems: PropTypes.array
}   

const mapStateToProps = state => ({
    bookingItems: state.bookings.bookingItems,
    modalOpen: state.modal.modalOpen,
    modalType: state.modal.modalType,
});

export default connect( mapStateToProps, { fetchBookings, selectedBookingData, openModal, setModalType } )(BookingCalendar);