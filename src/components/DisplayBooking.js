import { useSelector } from "react-redux";

const DisplayBooking = () => {
    const bookingItem = useSelector(state => state.bookings.selectedBookingData);
    const startDate = new Date(bookingItem.start).toLocaleString();
    const endDate = new Date(bookingItem.end).toLocaleString();

    return(
        <div className="display-booking">
            <h2 className="booking-title">{bookingItem.title}</h2>
            <p className="booking-start">Start Time: {startDate}</p>
            <p className="booking-end">End Time: {endDate}</p>
            <div className="booking-attendees">
                {
                    bookingItem.attendees.length
                    ? (<p>Attendees</p>)
                    : null
                }
                <ul>
                    {
                        bookingItem.attendees.map((attendee,index) => {
                            return (
                                <li key={index}>
                                    <p>{attendee.name}</p>
                                    <p>{attendee.email}</p>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>       
        </div>
    );
}

export default DisplayBooking;