const Loader = () => (
    <div className="loader-container">
        <div className="loader">
            <i className="fa fa-cog fa-spin fa-3x fa-fw"></i>
        </div>
    </div>
)
export default Loader;