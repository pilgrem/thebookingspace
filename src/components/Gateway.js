import { connect, useDispatch } from "react-redux";
import { getAccess, setAccess } from '../actions/gatewayActions';

const Gateway = props => {
    const dispatch = useDispatch();

    const accessSubmission = (e) => {
        e.preventDefault();
        const form = e.target;
        const codeInput = form.querySelector('input[type="password"]')
        const accessCode = codeInput.value;
        if (accessCode.length) {
            dispatch(setAccess(accessCode));
            if (!process.access) {
                if (!codeInput.classList.contains('error')) {
                    codeInput.classList.add('error');
                }                
            }
        }         
    }

    return (
        <div className="gateway">
            <form onSubmit={accessSubmission}>
                <div className="gateway-access-code">
                    <label>Enter Access Code:
                        <input type="password" />
                    </label>
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = (state) => ({
    access: state.gateway.access,
});

export default connect( mapStateToProps, { setAccess, getAccess } )(Gateway);
