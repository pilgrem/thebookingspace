const Notfound = () => {
    return (
        <article id="notfound" className="notfound">
            <p style={{
                fontSize: "75px",
                opacity: ".85",
                width: "100%",
                margin: "0 auto",
            }}>404 Page Not Found.</p>
        </article>
    )
}

export default Notfound
