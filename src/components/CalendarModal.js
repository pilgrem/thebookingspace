import { useSelector, useDispatch } from "react-redux";
import { closeModal, setModalType } from '../actions/modalActions';
import BookingForm from '../components/BookingForm';
import DisplayBooking from '../components/DisplayBooking';
import EditBooking from "../components/EditBooking";

const CalendarModal = () => {
    const dispatch = useDispatch();
    const show = useSelector(state => state.modal.modalOpen);
    const modalType = useSelector(state => state.modal.modalType);
    const showHideClose = show ? "modal display-block" : "modal display-none";
    
    const onEditBookingClick = () => {
        dispatch(setModalType('editBooking'));
    }

    return(
        <div className={showHideClose}>
            <section className="modal-main">
                <div className="close-modal-container">
                    <button className="close-modal" onClick={() => dispatch(closeModal())}>
                       <i className="fas fa-times" aria-hidden="true"></i>
                    </button>
                </div>
                {(() => {
                    switch (modalType) {
                        case 'bookingForm':
                            return(
                                <BookingForm />
                            )
                        case 'displayBooking':
                            return (
                                <>
                                <DisplayBooking />
                                <button type="button" onClick={onEditBookingClick}>Edit Booking</button>
                                </>
                            )
                        case 'bookingFormDashboard':
                            return (
                                <BookingForm redirect={true} />
                            )
                        case 'editBooking':
                            return(
                                <EditBooking />
                            )
                        default:
                            return (
                                <p>Error: Nothing found.</p>
                            )
                    }
                })()}
            </section>
        </div>
    );
}

export default CalendarModal;