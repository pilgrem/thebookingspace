const Head = () => (
    <div className="heading">
        <h1>The Booking Space <span>|</span> <span>A Conference Room Scheduler</span></h1>
    </div>    
);

export default Head;