import React, { Component} from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { closeModal } from '../actions/modalActions';
import { updateBooking, updateDisplayedBookings, deleteBooking } from '../actions/bookingActions';
import DatePicker from 'react-datepicker';
import Attendee from '../common/attendee';
import { setFormMessage } from '../common/helper';
import Validation from '../common/validation';

class EditBooking extends Component {

    constructor(props) {
        super(props);

        this.state =  {
            id: '',
            title: '',
            start: '',
            end: '',
            attendees: [],
        }

        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeDateStart = this.onChangeDateStart.bind(this);
        this.onChangeDateEnd = this.onChangeDateEnd.bind(this);
        this.onAttendeeAdd = this.onAttendeeAdd.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleAttendeeChange = this.handleAttendeeChange.bind(this);
        this.deleteBooking = this.deleteBooking.bind(this);
    }

    componentDidMount() {
        this.setState({
            id: this.props.selectedBookingData.id,
            title: this.props.selectedBookingData.title,
            start: this.props.selectedBookingData.start,
            end: this.props.selectedBookingData.end,
            attendees: this.props.selectedBookingData.attendees,
        });

        this.setAttendeeChangeListner();        
    }

    setAttendeeChangeListner() {
        const attendees = document.querySelectorAll('.attendee-item');
        attendees.forEach(attendee => {
            attendee.addEventListener("change", this.handleAttendeeChange);
        });
    }


    onAttendeeAdd(e) {
        e.preventDefault();
        const attendee = new Attendee();
        const attendeeList = e.target.previousSibling;
        const attendeeListCount = attendeeList.children.length;
        attendee.parentObject = attendeeList;
        attendee.attendeeCount = attendeeListCount;
        attendee.add();
        this.setAttendeeChangeListner();
    }

    handleAttendeeChange(e) {        
        const form = e.target.form;

        // Build attendee object for state.
        const attendees = [];        
        let emptyInputData = 0;
        Object.keys(form.elements).forEach(key => {
            let element = form.elements[key];
            if (element.type === 'fieldset') {
                const fieldSetElements = element.children;
                const inputData = {};
                
                Object.values(fieldSetElements).map((input) => {
                    if (!input.value.length) {
                        emptyInputData++;
                    }
                    inputData[input.dataset.key] = input.value;
                    return inputData;
                });

                if (emptyInputData !== 2) {
                    attendees.push(inputData);    
                }                
            }
            emptyInputData = 0;
        });
       
        this.setState({attendees});
    }

    onChangeTitle(e) {
        this.setState({title: e.target.value});
    }

    onChangeDateStart(start) {
        this.setState({start});
    }

    onChangeDateEnd(end) {
        this.setState({end});
    }

    async onSubmit(e) {
        e.preventDefault();
        const form = e.target;
        const validationError = new Validation(this.state);
        const errorContainer = form.querySelector('.booking-error-validation');

        if (validationError.length) {
            // Add error messaging.
            if (!errorContainer.classList.contains('error')) {
                errorContainer.classList.add('error');
            }
            errorContainer.innerText = validationError[0];
        } else {
            // Clean up errors.
            if (errorContainer.classList.contains('error')) {
                errorContainer.classList.remove('error');
                errorContainer.innerText = '';
            }

            await this.props.updateBooking(this.state)
            .then(() => {
                if (this.props.updatedBookingStatus) {
                    setFormMessage(form, 'Booking Successfully Updated!');
                    
                    /**
                     * Update the calendar with new booking edits.
                     */
                    this.props.updateDisplayedBookings(this.props.allBookings, this.state);
                } else {
                    setFormMessage(form, 'Booking Did Not Successfully Update.');
                }
            })
            .finally(() => {
                setTimeout(() => {
                    this.props.closeModal();
                }, 1350);
            });    
        }   
    }

    async deleteBooking(e) {
        const form = e.target.previousSibling;
        const confirmDelete = window.confirm('Confirm Booking Removal.');
        if (confirmDelete) {
            const errorContainer = form.querySelector('.booking-error-validation');
            // Clean up errors.
            if (errorContainer.classList.contains('error')) {
                errorContainer.classList.remove('error');
                errorContainer.innerText = '';
            }


            // Delete the Booking.
            await this.props.deleteBooking(this.state)
                .then(() => {                    
                    if (this.props.bookingDeleteStatus) {
                        setFormMessage(form, 'Booking Successfully Deleted!');

                        /**
                         * Update the calendar with new booking edits.
                         */
                        this.props.updateDisplayedBookings(this.props.allBookings, this.state, true);
                    } else {
                        setFormMessage(form, 'Error while deleting booking information.');
                    }
                })
                .finally(() => {
                    setTimeout(() => {
                        this.props.closeModal();
                    }, 1350);
                }); 
        }
    }

    render() {

        return (
            <div className="booking-edit-form">                
                <form onSubmit={this.onSubmit}>
                    <h2>Edit booking information: </h2>
                    <p className="booking-error-validation"></p>                    
                    <div className="form-element">
                        <label for="booking-title">Title:<sup>*</sup></label>
                        <input
                            type="text" 
                            name="booking-edit-title"
                            value={this.state.title}
                            onChange={this.onChangeTitle}
                        /> 
                    </div>

                    <div className="form-element">
                        <label for="start-date-picker">Start:<sup>*</sup></label>
                        <DatePicker 
                                id="start-date-picker"
                                placeholderText="Start Date/Time"
                                showTimeSelect
                                dropdownMode="select"
                                dateFormat="M/d/Y h:mm aa"                
                                selected={this.state.start}
                                onChange={this.onChangeDateStart}
                            />
                    </div>


                    <div className="form-element">
                        <label for="end-date-picker">End:<sup>*</sup></label>
                        <DatePicker
                            id="end-date-picker"
                            placeholderText="End Date/Time"
                            showTimeSelect
                            dropdownMode="select"
                            dateFormat="M/d/Y h:mm aa"                    
                            selected={this.state.end}
                            onChange={this.onChangeDateEnd}
                        />
                    </div>

                    <div className="booking-attendees">
                        <p>Attendees:</p>
                        <div className="booking-attendee-list">
                            {
                                this.state.attendees.map((attendee, index) => {
                                    return(
                                        <fieldset name={`attendee-group-${index}`} key={index}>
                                            <input className="attendee-item" type="text" name={`attendee-name[${index}]`} placeholder="Name" data-key="name" defaultValue={attendee.name}/>
                                            <input className="attendee-item" type="email" name={`attendee-email[${index}]`} placeholder="Email" data-key="email" defaultValue={attendee.email} />
                                        </fieldset>
                                    );
                                })
                            }
                        </div>
                        <button onClick={this.onAttendeeAdd} className="add-attendee">+</button>
                    </div>   
                    <button type="submit">Update Booking</button>
                </form>

                <button className="delete-button" type="button" onClick={this.deleteBooking}>Delete Booking</button>
            </div>
        );
    }
}

EditBooking.propTypes = {
    updateBooking: PropTypes.func,
    closeModal: PropTypes.func,
    updateDisplayedBookings: PropTypes.func,
    deleteBooking: PropTypes.func,
    selectedBookingData: PropTypes.shape({
        id: PropTypes.string,
        title: PropTypes.string,
        start: PropTypes.string,
        end: PropTypes.string,
        attendees: PropTypes.array,
    }),
}

const mapStateToProps = state => ({
    selectedBookingData: state.bookings.selectedBookingData,
    updatedBookingStatus: state.bookings.bookingUpdated,
    allBookings: state.bookings.bookingItems,
    bookingDeleteStatus: state.bookings.bookingDeleted,
});

export default connect( mapStateToProps, { closeModal, updateBooking, updateDisplayedBookings, deleteBooking } )(EditBooking);