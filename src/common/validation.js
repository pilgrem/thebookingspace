/**
 * Custom validation object for Booking form fields.
 */
export default class Validation {
    state = {};

    constructor(state) {
        return this.runValidation(state);
    }

    runValidation(state) {
        this.state = state;

        const validationCheck = [
            this.validateEmptyFields(),
            this.validateCorrectDate()
        ];

        /**
         * Check invalidations in order of how they
         * are placed into the validationCheck method.
         */
        let currentInvalidation = [];
        validationCheck.every( message => {           
            if (message.length) {
                currentInvalidation = message;
                return false;
            }

            return true;
        });

        return currentInvalidation;
    }

    /**
     * Validates for empty fields.
     * @returns array
     */
    validateEmptyFields() {
        let errorMessage = [];

        if (this.state.title === '') {
            errorMessage = [...errorMessage, 'Title'];
        }
        if (this.state.start === '') {
            errorMessage = [...errorMessage, 'Start'];
        }
        if (this.state.end === '') {
            errorMessage = [...errorMessage, 'End'];
        }

        /**
         * Format error string message.
         */
        const errorLength = errorMessage.length;
        let bits = '';
        if (errorLength) {
            if(errorLength > 1) {                               
                bits = `${errorMessage.join(', ')} fields are required.`;
            } else {
                bits = `${errorMessage[0]} field is required.`;
            }
            errorMessage = [bits];
        }

        return errorMessage;
    }

    validateCorrectDate() {
        let errorMessage = [];
        const { start, end } = this.state;
        
        const currentdate = this.unixDate();
        const startDate = start.length | this.unixDate(start);
        const endDate = end.length | this.unixDate(end);

        if (startDate && endDate) {

            // Check if startDate is greater than todays date.
            if (startDate < currentdate) {
                errorMessage = [...errorMessage, 'Please pick a date greater or equal to todays date.'];
                return errorMessage;
            }

            // Check if date order is correct.
            if (startDate > endDate) {
                errorMessage = [...errorMessage, 'Oops! Looks like you got your date order incorrect. Please fix that.'];
                return errorMessage;
            }            
        }

        return errorMessage;
    }

    /**
     * Get Unix timestamp.
     * @param {Date} inputDate 
     * @returns 
     */
    unixDate(inputDate = null) {
        const dateFn = inputDate !== null ? new Date(inputDate) : new Date();
        return Math.round((dateFn).getTime() / 1000);
    }

}