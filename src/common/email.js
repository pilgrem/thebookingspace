export default class Email {    
    #host = process.env.REACT_APP_RELAY_HOST;
    #username = process.env.REACT_APP_RELAY_USERNAME;
    #password = process.env.REACT_APP_RELAY_PASSWORD;
    #fromEmail = process.env.REACT_APP_RELAY_FROM;

    // Public Properties To Set.
    title = '';
    startTime = '';
    endTime = '';
    attendees = [];
    name = '';
    to = '';

    bodyTemplate() {
        return `
            <p>
                <strong>
                Hi! You have an meeting booked via The Booking Space.<br/>
                Here are your details!
                </strong>
            </p>           
            
            <p>
                <strong>Title:</strong> ${this.title}<br/>
                <strong>Start:</strong> ${this.startTime}<br/>
                <strong>End:</strong> ${this.endTime}</p>
                ${this.buildAttendeeString()}

            <p><i>- The Booking Space | A Conference Room Scheduler</i></p>
        `;
    }

    buildAttendeeString() {
        let string = '';
        if (this.attendees.length) {
            let values = ['<strong>Attendees:</strong>'];            
            this.attendees.forEach((attendee) => {
                values = [...values, `<div>${attendee.name} : ${attendee.email}</div>`];
            })
            string = values.join(' ');
        }
        return string;
    }

    buildSubjectString() {
        let template = ['The Booking Space!'];
        if (this.name.length) {          
            template = [...template, this.name];
            template = [...template, `You have a meeting booked!`];
        } else {
            template = [...template, `You have a meeting booked!`];
        }
        return template.join(' ');
    }

    sendMail() {
        // Rate limit sending of email.
        setTimeout(() => {
            window.Email.send({
                Host: this.#host,
                Username: this.#username,
                Password: this.#password,
                To: this.to,
                From: this.#fromEmail,
                Subject: this.buildSubjectString(),
                Body: this.bodyTemplate(),
            }).catch((err) => {
                console.error(err);
            })
        }, 350);            
    }
}