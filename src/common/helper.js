
/**
 * Helper function to set a message in the Modal popup after form submission.
 * @param {object} form 
 * @param {string} message 
 */
export const setFormMessage = (form, message) => {
    if (!form.classList.contains('submission-updated')) {
        form.classList.add('submission-updated');
    }
    const msgOutput = document.createElement('p');
    msgOutput.innerText = message;
    msgOutput.className = 'submission-message';
    form.appendChild(msgOutput);
}

/**
 * Generates random string.
 * @returns string
 */
export const randomString = () => {
    const length = 50;
    let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let string = '';
    for (let i = 0; i < length; i++) {
        string += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return string;
}

/**
 * Return cookie value.
 * @param {string} name 
 * @returns 
 */
export const getCookieValue = name => {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}
