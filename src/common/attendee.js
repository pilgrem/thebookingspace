export default class Attendee {

    attendeeCount = 0;
    parentObject = {};

    add(){
        const fieldset = this.createFieldSet();
        fieldset.appendChild(this.createInputName());
        fieldset.appendChild(this.createInputEmail());
        this.parentObject.appendChild(fieldset);
    }

    createFieldSet() {
        const fieldSet = document.createElement('fieldSet');
        fieldSet.name = `attendee-group-${this.attendeeCount}`;
        return fieldSet;
    }

    createInputName() {
        const inputName = document.createElement('input');
        inputName.type = 'text';
        inputName.name = `attendee-name[${this.attendeeCount}]`;
        inputName.placeholder = 'Name';
        inputName.dataset.key = 'name';
        inputName.className = 'attendee-item';
        return inputName;
    }

    createInputEmail() {
        const inputEmail = document.createElement('input');
        inputEmail.type = 'email';
        inputEmail.name = `attendee-email[${this.attendeeCount}]`;
        inputEmail.placeholder = 'Email';
        inputEmail.dataset.key = 'email';
        inputEmail.className = 'attendee-item';
        return inputEmail;
    }

    remove(){}
}