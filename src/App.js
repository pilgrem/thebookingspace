import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Dashboard from './views/dashboard';
import Calendar from './views/calendar';
import Notfound from './components/Notfound';
import './sass/App.scss';


const App = () => {

  return (    
    <Router>
        <Switch>
          <Route exact path="/" ><Dashboard /></Route>
          <Route exact path="/calendar"><Calendar /></Route>
          <Route path="*"><Notfound /></Route>
        </Switch>
    </Router>
  );
}

export default App;
