import React, { useEffect } from 'react';
import { connect, useDispatch } from "react-redux";
import { PropTypes } from 'prop-types';
import { Redirect } from 'react-router-dom';
import Head from "../components/Head";
import BookingCalendar from "../components/BookingCalendar";
import { getAccess, pageLoaded } from '../actions/gatewayActions';
import Loader from '../components/Loader';

const Calendar = props => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getAccess());
        dispatch(pageLoaded());
    }, [dispatch]);

    const { access, isPageLoaded } = props;

    const renderPage = () => {
        if (access !== null && isPageLoaded !== null ) {
            if (access) {
                return <BookingCalendar />;
            } else {
                return <Redirect to="/" />;
            }
            
        } else {
            return <Loader />
        }
    }

    return (
        <main>
            <Head />
            {renderPage()}
        </main>
    );  
}

Calendar.propTypes = {
    access: PropTypes.any,
    isPageLoaded: PropTypes.any,
}

const mapStateToProps = (state) => ({
    access: state.gateway.access,
    isPageLoaded: state.gateway.loaded,
});

export default connect( mapStateToProps, {} )(Calendar);