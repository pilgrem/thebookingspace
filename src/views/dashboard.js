import React, { useEffect, useState } from 'react';
import { PropTypes } from 'prop-types';
import { connect, useDispatch } from "react-redux";
import { Redirect } from 'react-router-dom';
import Head from '../components/Head';
import CalendarModal from '../components/CalendarModal';
import Gateway from '../components/Gateway';
import { openModal, setModalType } from '../actions/modalActions';
import { getAccess, pageLoaded } from '../actions/gatewayActions';
import Loader from '../components/Loader';

const Dashboard = props =>{
    const dispatch = useDispatch();
    const [state, setState] = useState({redirect: false});

    useEffect(() => {
        dispatch(getAccess());
        dispatch(pageLoaded());
    }, [dispatch]);

    const handleAddBooking = () => {
        dispatch(setModalType('bookingFormDashboard'));
        dispatch(openModal());
    };

    const handleViewBookingCalendar = () => {
        setState({redirect: true});
    }

    const { access, isPageLoaded } = props;

    const renderPage = () => {
        if(access !== null && isPageLoaded !== null ) {
            if (access) {
                return (
                    <div className="dashboard-container">
                        <div className="dashboard-options">
                            <button onClick={handleViewBookingCalendar}>View Booking Calendar</button> 
                            <button onClick={handleAddBooking}>Add Booking</button>
                        </div>
                        <CalendarModal />
                    </div>
                );
            } else {
                return (
                    <div className="dashboard-container">
                        <Gateway />
                    </div>
                );
            }
        } else {
            return <Loader />;
        }
    }

    return (
        <main>
            <Head />
            {renderPage()}
            {
                state.redirect
                ? <Redirect to="/calendar" />
                : null
            }
        </main>
    );

}

Dashboard.propTypes = {
    access: PropTypes.any,
    isPageLoaded: PropTypes.any,
}

const mapStateToProps = (state) => ({
    access: state.gateway.access,
    isPageLoaded: state.gateway.loaded,
});

export default connect( mapStateToProps, {} )(Dashboard);
